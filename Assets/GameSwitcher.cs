﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSwitcher : MonoBehaviour {

	public MinigameModule mm;

	public void SelectScene()
	{
		Debug.Log (mm.active_game);
		if (mm.active_game == "SpinWheel") {
			SceneManager.LoadScene ("SpinWheel");
		}

		else if (mm.active_game == "Roulette") {
			SceneManager.LoadScene ("Roulette");
		}

		else if (mm.active_game == "CardMatcher") {
			SceneManager.LoadScene ("CardMatch");
		}

		else if (mm.active_game == "Scratch-Off") {
			SceneManager.LoadScene ("newScratchOff");
		}
		else if (mm.active_game == "LotsDeals") {
			SceneManager.LoadScene ("LotsDeals");
		}
	}
}
