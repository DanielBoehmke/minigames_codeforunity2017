﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseModuleID : MonoBehaviour {

	public static ChooseModuleID instance = null;

	public int ModuleID = 13723;

	void Awake()
	{
		//check if instance already exists
		if (instance == null){
			instance = this; //if not, set this to instance
		}else if (instance != this){//if it already exists
			Destroy (gameObject);//destroy this game object.  This enforces our singleton pattern, meaing there can only be one ever instance of this script
		}

		DontDestroyOnLoad (gameObject);
	}
}
