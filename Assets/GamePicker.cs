﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GamePicker : MonoBehaviour
{
    public string scene;
    public ScratchUIManager ui;

    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (SceneManager.GetActiveScene().name.Equals("newScratchOff"))
            {
                if (!ui.canPlay) { 
                Debug.Log("Scene Clicked: " + scene);
                SceneManager.LoadScene(scene);
                }
            }
            else
            {
                Debug.Log("Scene Clicked: " + scene);
                SceneManager.LoadScene(scene);
            }
        }
    }

}
