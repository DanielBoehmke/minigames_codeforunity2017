﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseSalonID : MonoBehaviour {

	public static ChooseSalonID instance = null;

	public int salonID = 185;

	void Awake()
	{
		//check if instance already exists
		if (instance == null){
			instance = this; //if not, set this to instance
		}else if (instance != this){//if it already exists
			Destroy (gameObject);//destroy this game object.  This enforces our singleton pattern, meaing there can only be one ever instance of this script
		}
			
		DontDestroyOnLoad (gameObject);
	}
}
