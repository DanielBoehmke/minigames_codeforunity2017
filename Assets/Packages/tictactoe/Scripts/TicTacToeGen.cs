﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TicTacToeGen : MonoBehaviour
{
    public GameObject tile;
    public TextMesh text;
    public GameObject back;

    public Board tiles;
    public static bool turn;
    public static bool playing;
    public static bool wait;
    public static int turns;
    public static int move;

    private int[] moves;

    // Use this for initialization
    void Start()
    {
        turns = 0;
        text.text = "Lets start playing.";
        turn = true;
        playing = true;
        back.SetActive(false);
        moves = new int[9];

        //Debug.Log("Outcome :" + GetBestMove(tiles,0));
        //Debug.Log("Outcome: " + FindWin(tiles, 0));
    }

    // Update is called once per frame
    void Update()
    {
        if (tiles)
        {
            //Debug.Log("Turns: " + turns);
            Debug.Log("Turn: " + turn);
            //ShowTiles(tiles);
        }

        if (playing)
        {

            if (!turn && !FindWin(tiles, 1) && !wait)
            {
                wait = true;
                int getMove = GetCrappyMove(move);
                while(getMove == -1)
                {
                    getMove = GetCrappyMove(move);
                }
                StartCoroutine(PlayO(getMove));
                
                //Debug.Log("Outcome :" + GetBestMove(tiles, 0));
            }
            else
            {
                if (FindWin(tiles, 1))
                {
                    text.text = "You Win";
                    Debug.Log("You win");
                    playing = false;
                }
                if (FindWin(tiles, 0))
                {
                    text.text = "You Lost";
                    Debug.Log("You lost");
                    playing = false;
                }
            }
        }
        else
        {
            back.SetActive(true);
        }
        if (turns == 9)
        {
            if (FindWin(tiles, 1))
            {
                text.text = "You Win";
                Debug.Log("You win");
                playing = false;
            }
            else
            {
                text.text = "It's a tie.";
            }
        }
    }

    void OnMouseOver()
    {

    }

    void ShowTiles(Board tempTiles)
    {
        string tempStr = "";
        int count = 0;
        for (int y = 0; y < 3; y++)
        {
            for (int x = 0; x < 3; x++)
            {
                tempStr += tempTiles.GetTile(count).GetX();
                count++;
            }
            tempStr += "\n";
        }
        tempStr += "\n";
        Debug.Log(tempStr);
    }

    private bool FindWin(Board testBoard, int player)
    {
        if (testBoard)
        {
            if (testBoard.GetTile(0).GetX() == player &&
                testBoard.GetTile(1).GetX() == player &&
                testBoard.GetTile(2).GetX() == player)
            {
                return true;
            }
            else if (testBoard.GetTile(3).GetX() == player &&
              testBoard.GetTile(4).GetX() == player &&
              testBoard.GetTile(5).GetX() == player)
            {
                return true;
            }
            else if (testBoard.GetTile(6).GetX() == player &&
               testBoard.GetTile(7).GetX() == player &&
               testBoard.GetTile(8).GetX() == player)
            {
                return true;
            }
            else if (testBoard.GetTile(0).GetX() == player &&
               testBoard.GetTile(3).GetX() == player &&
               testBoard.GetTile(6).GetX() == player)
            {
                return true;
            }
            else if (testBoard.GetTile(1).GetX() == player &&
               testBoard.GetTile(4).GetX() == player &&
               testBoard.GetTile(7).GetX() == player)
            {
                return true;
            }
            else if (testBoard.GetTile(2).GetX() == player &&
               testBoard.GetTile(5).GetX() == player &&
               testBoard.GetTile(8).GetX() == player)
            {
                return true;
            }
            else if (testBoard.GetTile(0).GetX() == player &&
               testBoard.GetTile(4).GetX() == player &&
               testBoard.GetTile(8).GetX() == player)
            {
                return true;
            }
            else if (testBoard.GetTile(2).GetX() == player &&
               testBoard.GetTile(4).GetX() == player &&
               testBoard.GetTile(6).GetX() == player)
            {
                return true;
            }
        }
        return false;
    }

    IEnumerator PlayO(int tempInt)
    {
        Debug.Log("Play O Tried: " + +tempInt);
        if (tempInt != -1)
        {
            yield return new WaitForSeconds(1.5f);
            tiles.GetTile(tempInt).SetX(0);
            tiles.GetTile(tempInt).SetSprite(1);
            turn = true;
            wait = false;
            turns++;
        }
    }

    int GetUserNumber()
    {
        int userNum = 0;

        for (int i = 0; i < tiles.GetSize(); i++)
        {
            if (tiles.GetTile(i).GetX() == 1)
            {
                userNum++;
            }
        }

        return userNum;
    }

    int GetPossibleMovesNumber()
    {
        int tempInts = 0;

        for (int i = 0; i < tiles.GetSize(); i++)
        {
            if (tiles.GetTile(i).GetX() == -1)
            {
                tempInts++;
            }
        }

        return tempInts;
    }

    int[] GetPossibleMoves()
    {
        List<int> tempInts = new List<int>();
        for (int i = 0; i < tiles.GetSize(); i++)
        {
            if (tiles.GetTile(i).GetX() == -1)
            {
                tempInts.Add(i);
            }
        }

        return tempInts.ToArray();
    }

    int GetBestMove(Board testingTiles, int player)
    {
        Board testTiles = gameObject.AddComponent<Board>();
        testTiles = testingTiles;
        int[] avaliableMoves = GetPossibleMoves();
        int[,] bestMove = new int[10, 10];

        if (FindWin(testTiles, 1))
        {
            return -10;
        }
        else if (FindWin(testTiles, 0))
        {
            return 10;
        }
        else if (GetPossibleMovesNumber() == 0)
        {
            return 0;
        }

        for (int i = 0; i < avaliableMoves.Length; i++)
        {
            if (player == 0)
            {
                bestMove[i, 0] = avaliableMoves[i];
                //bestMove[i, 1] = GetBestMove(testTiles, 0);
            }
            else
            {
                bestMove[i, 0] = avaliableMoves[i];
                //bestMove[i, 1] = GetBestMove(testTiles, 1);
            }
        }

        for (int i = 0; i < bestMove.Length; i++)
        {
            Debug.Log("Number at : " + i + " is " + bestMove[i, 0]);
        }
        return 0;
    }

    int GetCrappyMove(int num)
    {
        Debug.Log("Get Crappy Move: " + num);
        switch (num)
        {
            case 0:
                switch (Random.Range(0, 2))
                {
                    case 0:
                        if (tiles.GetTile(1).GetX() == -1)
                        {
                            return 1;
                        }
                        break;
                    case 1:
                        if (tiles.GetTile(3).GetX() == -1)
                        {
                            return 3;
                        }
                        break;
                    case 2:
                        if (tiles.GetTile(4).GetX() == -1)
                        {
                            return 4;
                        }
                        break;
                }
                break;
            case 1:
                switch (Random.Range(0, 4))
                {
                    case 0:
                        if (tiles.GetTile(0).GetX() == -1)
                        {
                            return 0;
                        }
                        break;
                    case 1:
                        if (tiles.GetTile(3).GetX() == -1)
                        {
                            return 3;
                        }
                        break;
                    case 2:
                        if (tiles.GetTile(4).GetX() == -1)
                        {
                            return 4;
                        }
                        break;
                    case 3:
                        if (tiles.GetTile(5).GetX() == -1)
                        {
                            return 5;
                        }
                        break;
                    case 4:
                        if (tiles.GetTile(2).GetX() == -1)
                        {
                            return 2;
                        }
                        break;
                }
                break;
            case 2:
                switch (Random.Range(0, 2))
                {
                    case 0:
                        if (tiles.GetTile(1).GetX() == -1)
                        {
                            return 1;
                        }
                        break;
                    case 1:
                        if (tiles.GetTile(4).GetX() == -1)
                        {
                            return 4;
                        }
                        break;
                    case 2:
                        if (tiles.GetTile(5).GetX() == -1)
                        {
                            return 5;
                        }
                        break;
                }
                break;
            case 3:
                switch (Random.Range(0, 4))
                {
                    case 0:
                        if (tiles.GetTile(0).GetX() == -1)
                        {
                            return 0;
                        }
                        break;
                    case 1:
                        if (tiles.GetTile(1).GetX() == -1)
                        {
                            return 1;
                        }
                        break;

                    case 2:
                        if (tiles.GetTile(4).GetX() == -1)
                        {
                            return 4;
                        }
                        break;
                    case 3:
                        if (tiles.GetTile(7).GetX() == -1)
                        {
                            return 7;
                        }
                        break;
                    case 4:
                        if (tiles.GetTile(6).GetX() == -1)
                        {
                            return 6;
                        }
                        break;
                }
                break;
            case 4:
                switch (Random.Range(0, 7))
                {
                    case 0:
                        if (tiles.GetTile(0).GetX() == -1)
                        {
                            return 0;
                        }
                        break;
                    case 1:
                        if (tiles.GetTile(1).GetX() == -1)
                        {
                            return 1;
                        }
                        break;
                    case 2:
                        if (tiles.GetTile(2).GetX() == -1)
                        {
                            return 2;
                        }
                        break;
                    case 3:
                        if (tiles.GetTile(3).GetX() == -1)
                        {
                            return 3;
                        }
                        break;
                    case 4:
                        if (tiles.GetTile(5).GetX() == -1)
                        {
                            return 5;
                        }
                        break;
                    case 5:
                        if (tiles.GetTile(6).GetX() == -1)
                        {
                            return 6;
                        }
                        break;
                    case 6:
                        if (tiles.GetTile(7).GetX() == -1)
                        {
                            return 7;
                        }
                        break;
                    case 7:
                        if (tiles.GetTile(8).GetX() == -1)
                        {
                            return 8;
                        }
                        break;
                }
                break;
            case 5:
                switch (Random.Range(0, 4))
                {
                    case 0:
                        if (tiles.GetTile(1).GetX() == -1)
                        {
                            return 1;
                        }
                        break;
                    case 1:
                        if (tiles.GetTile(2).GetX() == -1)
                        {
                            return 2;
                        }
                        break;
                    case 2:
                        if (tiles.GetTile(4).GetX() == -1)
                        {
                            return 4;
                        }
                        break;
                    case 3:
                        if (tiles.GetTile(7).GetX() == -1)
                        {
                            return 7;
                        }
                        break;
                    case 4:
                        if (tiles.GetTile(8).GetX() == -1)
                        {
                            return 8;
                        }
                        break;

                }
                break;
            case 6:
                switch (Random.Range(0, 2))
                {
                    case 0:
                        if (tiles.GetTile(3).GetX() == -1)
                        {
                            return 3;
                        }
                        break;
                    case 1:
                        if (tiles.GetTile(4).GetX() == -1)
                        {
                            return 4;
                        }
                        break;
                    case 2:
                        if (tiles.GetTile(7).GetX() == -1)
                        {
                            return 7;
                        }
                        break;
                }
                break;
            case 7:
                switch (Random.Range(0, 4))
                {
                    case 0:
                        if (tiles.GetTile(6).GetX() == -1)
                        {
                            return 6;
                        }
                        break;
                    case 1:
                        if (tiles.GetTile(3).GetX() == -1)
                        {
                            return 3;
                        }
                        break;
                    case 2:
                        if (tiles.GetTile(4).GetX() == -1)
                        {
                            return 4;
                        }
                        break;
                    case 3:
                        if (tiles.GetTile(5).GetX() == -1)
                        {
                            return 5;
                        }
                        break;
                    case 4:
                        if (tiles.GetTile(8).GetX() == -1)
                        {
                            return 8;
                        }
                        break;
                }
                break;
            case 8:
                switch (Random.Range(0, 2))
                {
                    case 0:
                        if (tiles.GetTile(4).GetX() == -1)
                        {
                            return 4;
                        }
                        break;
                    case 1:
                        if (tiles.GetTile(5).GetX() == -1)
                        {
                            return 5;
                        }
                        break;
                    case 2:
                        if (tiles.GetTile(7).GetX() == -1)
                        {
                            return 7;
                        }
                        break;
                }
                break;
            default:
                return -1;
        }
        return -1;
    }
}
