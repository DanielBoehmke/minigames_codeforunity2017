﻿using UnityEngine;
using System.Collections;

public class Board : MonoBehaviour
{
    public Tile[] tiles;
    public Tile tile;
    private int count;

    // Use this for initialization
    void Start()
    {
        tiles = new Tile[9];
        BuildGrid();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public Tile GetTile(int num)
    {
        //Debug.Log("Get Tile: " + num);
        return tiles[num];
    }

    void BuildGrid()
    {
        int count = 0;
        for (int y = 0; y < 3; y++)
        {
            for (int x = 0; x < 3; x++)
            {
                Tile tempTile = Instantiate(tile, new Vector3((x * 1.7f) + -1.7f, 1.7f - (y * 1.7f), 0), Quaternion.identity);
                tempTile.transform.SetParent(this.transform);
                tempTile.SetI(count);
                tiles[count] = tempTile;
                count++;
            }
        }
    }

    public int GetSize()
    {
        return tiles.Length;
    }
}
