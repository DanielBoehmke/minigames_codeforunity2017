﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour {

    public Sprite[] sprites;
    private int x;

    private int i;

	// Use this for initialization
	void Start () {
        x = -1;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (TicTacToeGen.turn && TicTacToeGen.playing && x == -1)
            {
                this.GetComponent<SpriteRenderer>().sprite = sprites[2];
                TicTacToeGen.move = i;
                x = 1;
                TicTacToeGen.turn = false;
                TicTacToeGen.turns++;
            }
        }
    }

    public int GetX()
    {
        return x;
    }

    public void SetX(int x)
    {
        this.x = x;
    }

    public void SetI(int i)
    {
        this.i = i;
    }

    public void SetSprite(int num)
    {
        this.GetComponent<SpriteRenderer>().sprite = sprites[num];
    }
}
