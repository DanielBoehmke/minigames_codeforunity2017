﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BriefCaseManager : MonoBehaviour {

	public GameObject briefCase;
	public static bool picked;
	public bool pause;
	public GameObject topText;
	public static int round; 
	public static int count;

    public Sprite[] images;

	private List<int> values;
	private static List<int> randValues;
	private List<int> currentValues;

	// Use this for initialization
	void Start () {
		values = new List<int> {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18};

		randValues = reshuffle (values);

		string tempString = "";
		for (int i = 0; i < randValues.Count; i++) {
			tempString += randValues[i] + ", ";
		}

		//Debug.Log (tempString);
		round = 1;
        count = 0;
        picked = false;

		BuildGrid ();
	}
	
	// Update is called once per frame
	void Update () {
        //Debug.Log (getAverage ());
        //Debug.Log("Picked: " + picked);
		displayNums ();
	}

	List<int> reshuffle(List<int> images){
		int n = images.Count;
		while (n > 1) {
			n--;
			int k = Random.Range (0, images.Count);
			int value = images [k];
			images [k] = images [n];
			images [n] = value;
		}
		return images;
	}

	void BuildGrid(){
		int count = 0;
		for (int y = 0; y < 6; y++) {
			for (int x = 0; x < 3; x++) {
				count++;
				GameObject tempCase = Instantiate (briefCase, new Vector3 ((x * 2.0f) + -2.0f, 4.5f - (y * 1.5f) , 0), Quaternion.identity);
				tempCase.transform.SetParent (this.transform);
				tempCase.GetComponentInChildren<BriefCase> ().setCount(count);
				tempCase.GetComponentInChildren<BriefCase> ().setRandValue (randValues [count-1]);
				tempCase.GetComponentInChildren<BriefCase> ().setValue(randValues[count-1] * 50000);
				tempCase.GetComponentInChildren<TextMesh>().text = "" + count; 
			}
		}
	}

	public static void removeItem(int num){
		//Debug.Log ("Removed: " + randValues[num]);
		randValues.Remove(num);
	}

	public static void addItem(int num){
		randValues.Add (num);
	}

	public void setRound(int num){
		round = num;
	}

	public int getRound(){
		return round;
	}

	public void setCount(int num){
		count = num;
	}

	public int getCount(){
		return count;
	}

	public float getAverage(){
		float tempInt = 0;
		for (int i = 0; i < randValues.Count; i++) {
			tempInt += randValues[i];
			//Debug.Log (tempInt);
		}

		float tempFloat = (tempInt / randValues.Count) * 50000f;

		return tempFloat;
	}

	void displayNums(){
		string tempString = "";
		for (int i = 0; i < randValues.Count; i++) {
			tempString += randValues[i] + ", ";
		}

		//Debug.Log (tempString);
	}

	public void setText(int num){
        topText.GetComponent<SpriteRenderer>().sprite = images[num];
		//topText.text = str;
	}

	public string offerBid(){
		float tempFloat;
        int tempInt;

		switch (round) {
		case 1:
			tempFloat = getAverage () * Random.Range (.1f, .3f);
            Debug.Log("Temp Float: " + tempFloat);
            tempFloat = tempFloat / 1000;
            Debug.Log("Temp Float: " + tempFloat);
            tempInt = (int) tempFloat * 1000;
            Debug.Log("Temp Int: " + tempInt);
			return tempInt.ToString("F0");
		case 2:
            tempFloat = getAverage() * Random.Range(.3f, .5f);
            tempFloat = tempFloat / 1000;
            tempInt = (int)tempFloat * 1000;
            return tempInt.ToString("F0");
		case 3:
			tempFloat = getAverage () * Random.Range (.5f, .7f);
            tempFloat = tempFloat / 1000;
            tempInt = (int)tempFloat * 1000;
            return tempInt.ToString("F0");
		case 4:
			tempFloat = getAverage () * Random.Range (.7f, .9f);
            tempFloat = tempFloat / 1000;
            tempInt = (int)tempFloat * 1000;
            return tempInt.ToString("F0");
		case 5:
			tempFloat = getAverage () * Random.Range (.9f, 1.1f);
            tempFloat = tempFloat / 1000;
            tempInt = (int)tempFloat * 1000;
            return tempInt.ToString("F0");
		default:
			return "-1";
			//break;
		}
	}
}
