﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LotsOfDealsGen : MonoBehaviour {

	//public static GameObject messageBox;
	public static  GameObject current;
	public static string offer;
	public static string playerValue;
	public static GameObject playerCase;

	void Awake (){
		DontDestroyOnLoad (gameObject);
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log ("Picked: " + BriefCaseManager.picked);
	}

	public static void CreateBox(BriefCase temp, string msg, string type){
		GameObject tempBox = Instantiate (Resources.Load("MessageBox") as GameObject, new Vector3 (0,0 ,-7), Quaternion.identity);
		tempBox.GetComponentInChildren<TextMesh> ().text = (msg);
	    tempBox.GetComponentInChildren<YesBox> ().type = type;
		if (temp) {
			current = temp.gameObject;
		}
	}
}
