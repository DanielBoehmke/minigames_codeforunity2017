﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class YesBox : MonoBehaviour {

	public string button;
	public string type;
    public GameObject text;
	public GameObject box;
	public GameObject yes;
	public GameObject no;
    public GameObject ok;
    public GameObject valueOk;
	public GameObject current;
	public GameObject loadingBar;
	public BriefCaseManager gameGen;
    public SpriteRenderer menuSprite;

    public Sprite sure;
    public Sprite value;
    public Sprite banker;
    public Sprite final;
    public Sprite win;

	// Use this for initialization
	void Start () {
		BriefCaseManager.picked = true;
		loadingBar = GameObject.Find("LoadingBar");
		gameGen = GameObject.Find ("BriefCaseManager").GetComponentInChildren<BriefCaseManager> ();
        if (type != "")
        {
            if (type == "Value")
            {
                Debug.Log("Value");
                //yes.GetComponentInChildren<TextMesh> ().text = "OK";
                text.SetActive(true);
                no.SetActive(false);
                yes.SetActive(false);
                ok.SetActive(false);
                valueOk.SetActive(true);
                menuSprite.sprite = value;
            }
            else if (type == "Offer")
            {
                text.SetActive(true);
                text.transform.position = new Vector3(0, 0.2f, text.transform.position.z);
                text.transform.localScale = new Vector3(0.1f, 0.1f, 1);
                no.SetActive(true);
                yes.SetActive(true);
                ok.SetActive(false);
                valueOk.SetActive(false);
                menuSprite.sprite = banker;
            }
            else if (type == "Final")
            {
                text.SetActive(true);
                text.transform.position = new Vector3(0, 0.2f, text.transform.position.z);
                text.transform.localScale = new Vector3(0.1f, 0.1f, 1);
                no.SetActive(true);
                yes.SetActive(true);
                ok.SetActive(false);
                valueOk.SetActive(false);
                menuSprite.sprite = final;
            }
            else if (type == "Win")
            {
                text.SetActive(false);
                no.SetActive(false);
                yes.SetActive(false);
                ok.SetActive(false);
                valueOk.SetActive(true);
                valueOk.transform.position = new Vector3(0,-0.8f,0);
                menuSprite.sprite = win;
            }
            else
            {
                Debug.Log("Sure");
                text.SetActive(false);
                no.SetActive(true);
                yes.SetActive(true);
                ok.SetActive(false);
                valueOk.SetActive(false);
                menuSprite.sprite = sure;
            }
        }
		Debug.Log ("Round: " + gameGen.getRound ());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnMouseOver(){
		if (Input.GetMouseButtonDown (0)) {
			if (button == "Yes") {
				//box.gameObject.SetActive(false);
				if (transform.parent.GetComponent<YesBox>().type == "Sure" && gameGen.getRound() == 1) {
					LotsOfDealsGen.current.GetComponentInChildren<BriefCase> ().setPlayerCase(true);
					LotsOfDealsGen.playerValue = LotsOfDealsGen.current.GetComponentInChildren<BriefCase> ().getValue().ToString();
					LotsOfDealsGen.playerCase = LotsOfDealsGen.current;
					Debug.Log ("Player Value: " + LotsOfDealsGen.playerValue);
					gameGen.setText (7);
					gameGen.setCount (7);
					Destroy (transform.parent.gameObject);
				}
				if (transform.parent.GetComponent<YesBox>().type == "Value") {
					if (gameGen.getCount() == 0) {
						loadingBar.GetComponentInChildren<LoadingBar>().setTimes (5);
					}
					gameGen.pause = false;
					BriefCaseManager.removeItem (LotsOfDealsGen.current.GetComponent<BriefCase>().getRandValue ());
					Destroy (LotsOfDealsGen.current);
					Destroy (transform.parent.gameObject);
				}
				if (transform.parent.GetComponent<YesBox> ().type == "Offer" || transform.parent.GetComponent<YesBox> ().type == "Final") {
					Destroy (transform.parent.gameObject);
					LotsOfDealsGen.CreateBox (null, "Congratulations\nyou win\n" + LotsOfDealsGen.offer,"Win");
					LotsOfDealsGen.playerCase.GetComponent<BriefCase>().show ();
				}
				if (transform.parent.GetComponent<YesBox> ().type == "Win") {
                    SceneManager.LoadScene("LotsDeals");
                    Destroy (transform.parent.gameObject);
				}


				//loadingBar.GetComponentInChildren<LoadingBar>().setTimes (5);
				//BriefCaseManager.picked = false;
			}
			if (button == "No") {
				if (transform.parent.GetComponent<YesBox> ().type == "Sure" && gameGen.getRound () == 1) {
					LotsOfDealsGen.current.GetComponentInChildren<BriefCase> ().setReturn (true);
				}
				if (transform.parent.GetComponent<YesBox> ().type == "Offer" && gameGen.getRound() == 4) {
					gameGen.setText (1);
					gameGen.setCount (1);
					gameGen.setRound (5);
				}
				if (transform.parent.GetComponent<YesBox> ().type == "Offer" && gameGen.getRound() == 3) {
					gameGen.setText (1);
					gameGen.setCount (1);
					gameGen.setRound (4);
				}
				if (transform.parent.GetComponent<YesBox> ().type == "Offer" && gameGen.getRound() == 2) {
					gameGen.setText (3);
					gameGen.setCount (3);
					gameGen.setRound (3);
				}
				if (transform.parent.GetComponent<YesBox> ().type == "Offer" && gameGen.getRound() == 1) {
					gameGen.setText (5);
					gameGen.setCount (5);
					gameGen.setRound (2);
				}
				if(transform.parent.GetComponent<YesBox> ().type == "Final") {
					LotsOfDealsGen.CreateBox (null, "Congratulations\nyou win\n" + LotsOfDealsGen.playerValue,"Win");
				}
				Destroy (transform.parent.gameObject);
			}
		}
	}
}
