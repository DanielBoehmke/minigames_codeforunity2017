﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingBar : MonoBehaviour {

	public Image cooldown;
	public bool coolingDown;
	public float waitTime = 10.0f;
	public int times = 5;
	public Text text;
	public GameObject coolGroup;
	public BriefCaseManager gameGen;


	// Use this for initialization
	void Start () {
		cooldown.fillAmount = 0;	
		gameGen = GameObject.Find ("BriefCaseManager").GetComponentInChildren<BriefCaseManager> ();
	}
	
	// Update is called once per frame
	void Update()
	{
		if (coolingDown == true) {
			if (times > 0) { 
				coolGroup.SetActive (true);
				//Reduce fill amount over 30 seconds
				cooldown.fillAmount += 1.0f / waitTime * Time.deltaTime;
				if (cooldown.fillAmount == 1) {
					cooldown.fillAmount = 0;
					times--;
					text.text = "" + times;
				}
			} else {
				text.text = "5";
				coolingDown = false;
				coolGroup.SetActive (false);
				LotsOfDealsGen.offer = gameGen.offerBid ();
				LotsOfDealsGen.CreateBox (null, "" + LotsOfDealsGen.offer,"Offer");
			}
		} else {
			coolGroup.SetActive (false);
		}
	}

	public void setTimes(int num){
		times = num;
		coolingDown = true;
	}
}