﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BriefCase : MonoBehaviour {

	public Vector3 target;
	public Vector3 start;
	public float speed;
	public GameObject valueText;
	private bool clicked;
	private bool returnCase;
	private bool moveToMiddle;
	private bool playersCase;
	public int count;
	public int value;
	public int randValue;

	public BriefCaseManager gameGen;

	// Use this for initialization
	void Start () {
		start = transform.position;
		playersCase = false;
		gameGen = GameObject.Find ("BriefCaseManager").GetComponentInChildren<BriefCaseManager> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!playersCase) {
			if (clicked) {
				float step = speed + Time.deltaTime;
				transform.position = Vector3.MoveTowards (transform.position, target, step);
			} else if (returnCase) {
				float step = speed + Time.deltaTime;
				transform.position = Vector3.MoveTowards (transform.position, start, step);
			} else if (moveToMiddle) {
				float step = speed + Time.deltaTime;
				transform.position = Vector3.MoveTowards (transform.position, new Vector3 (0, 0, -1), step);
			}
		}
	}

	void OnMouseOver(){
		if (Input.GetMouseButtonDown (0)) {
			if (!BriefCaseManager.picked) {
				BriefCaseManager.picked = true;
				transform.position = new Vector3 (transform.position.x, transform.position.y, -1);
				//playersCase = true;
				clicked = true;
				returnCase = false;
				moveToMiddle = false;
				LotsOfDealsGen.CreateBox (this,"Are you sure?","Sure");
				BriefCaseManager.removeItem (randValue);
				//Debug.Log ("Clicked");
			}
			if (gameGen.getCount () > 0 && !gameGen.pause && !playersCase && gameGen.getRound() < 5) {
				gameGen.setCount (gameGen.getCount () - 1);
				if (gameGen.getCount () > 1) {
					gameGen.setText (gameGen.getCount ());
				} else if (gameGen.getCount () == 1) {
					gameGen.setText (gameGen.getCount ());
				} else {
					gameGen.setText (8);

				}

				GameObject textMesh = Instantiate (valueText, start, Quaternion.identity); 
				textMesh.GetComponent<TextMesh>().text = value.ToString();
				gameGen.pause = true;
				Debug.Log ("Count: " + gameGen.getCount ());
				clicked = false;
				returnCase = false;
				moveToMiddle = true;
				LotsOfDealsGen.CreateBox (this, "" + getValue (),"Value");
			}
			if (gameGen.getRound () == 5) {
				LotsOfDealsGen.offer = getValue ().ToString();
				LotsOfDealsGen.CreateBox (this, "" + getValue (),"Final");
			}
		}

	}

	public void setPlayerCase(bool boo){
		playersCase = boo;
		start = new Vector3 (0,-5,-1);
	}

	public void setValue(int num){
		value = num;
	}

	public int getValue(){
		return (value);
	}

	public void setRandValue(int num){
		randValue = num;
	}

	public int getRandValue(){
		return (randValue);
	}

	public void setCount(int num){
		count = num;
	}

	public int getCount(){
		return (count);
	}

	public void setReturn(bool tmp){
		clicked = false;
		playersCase = false;
		returnCase = tmp;
		BriefCaseManager.addItem (randValue);
		BriefCaseManager.picked = false;
	}

	public void show(){
		GameObject textMesh = Instantiate (valueText, start, Quaternion.identity); 
		textMesh.GetComponent<TextMesh>().text = value.ToString();
		Destroy (this.gameObject);
	}
}
