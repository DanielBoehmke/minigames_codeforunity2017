﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockPaper : MonoBehaviour {

    public GameObject hand;
    public GameObject menu;
    public GameObject playerHand;
    public GameObject gameHand;
    public static bool Playing;
    public static int playerType;
    public static int computerType;
    public int speed;

    private bool played;


	// Use this for initialization
	void Start () {
        CreateHand();
	}
	
	// Update is called once per frame
	void Update () {
        if (Playing)
        {
            menu.SetActive(false);
            if (!played)
            {
                played = true;
                playerHand.GetComponent<Hand>().SetSprite(playerType);
                playerHand.SetActive(true);
                Debug.Log("Player Hand: " + playerType);
            }
            float step = speed + Time.deltaTime;
            //gameHand.transform.position = Vector3.MoveTowards(gameHand.transform.position, new Vector3(0,0,-1) , step);
            //playerHand.transform.position = Vector3.MoveTowards(playerHand.transform.position, new Vector3(0, 0, -1), step);
        }
    }

    void CreateHand()
    {
        int handType = Random.Range(0, 2);
        //Debug.Log("Hand Type " + handType);
        gameHand = Instantiate(hand, new Vector3(0, 4, -1), Quaternion.identity);
        gameHand.GetComponent<Hand>().SetSprite(handType);
    }

    void FindWin(int player, int computer)
    {

    }
}
