﻿using UnityEngine;
using System.Collections;

public class Hand : MonoBehaviour
{
    public int player;
    public int handType;
    public SpriteRenderer sprite;
    public Sprite[] sprites;

    private int count;

    // Use this for initialization
    void Start()
    {
        if (player == 0)
        {
            count = 0;
            StartCoroutine(RandomPicture());
        }
    }

    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //Debug.Log("The Type Number: " + handType);
            RockPaper.playerType = handType;
            RockPaper.Playing = true;
        }
    }

    public void SetSprite(int num)
    {
        //Debug.Log("Set Sprite " + num);
        sprite.sprite = sprites[num];
    }

    IEnumerator RandomPicture()
    {
        while (count < 1000 && !RockPaper.Playing)
        {
            yield return new WaitForSeconds(.1f);
            sprite.sprite = sprites[count % 3];
            count++;
            //Debug.Log("Count : " + (count % 3));
        }
        RockPaper.computerType = (count - 1)%3;
        Debug.Log("Computer Type: " + RockPaper.computerType);
    }
}
