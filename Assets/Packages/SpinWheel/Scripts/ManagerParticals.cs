﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerParticals : MonoBehaviour {

	public ParticleSystem psRed;
	public ParticleSystem psBlue;

	public void PlayParticals () 
	{
		Debug.Log ("CONFETTI");
		psRed.Play ();
		psBlue.Play ();
	}
}
